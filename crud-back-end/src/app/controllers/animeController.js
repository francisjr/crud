const express = require("express");

const authMiddleware = require("../middlewares/auth.js");

const Anime = require("../models/Anime.js");

const router = express.Router();

router.use(authMiddleware);

router.get("/", async (req, res) => {
  try {
    const animeList = await Anime.find({ user: req.userId });

    return res.send({ animeList });
  } catch (error) {
    return res.status(400).send({ error: "Error loading anime list." });
  }
});

router.get("/:animeId", async (req, res) => {
  try {
    const anime = await Anime.findById(req.params.animeId);

    return res.send({ anime });
  } catch (error) {
    return res.status(400).send({ error: "Error loading anime information." });
  }
});

router.post("/", async (req, res) => {
  try {
    const anime = await Anime.create({ ...req.body, user: req.userId });

    return res.send({ anime });
  } catch (error) {
    return res.status(400).send({ error: "Error add new anime to list." });
  }
});

router.put("/:animeId", async (req, res) => {
  try {
    const anime = await Anime.findByIdAndUpdate(req.params.animeId, req.body, {
      new: true,
    });

    return res.send({ anime });
  } catch (error) {
    return res.status(400).send({ error: "Error updating anime information." });
  }
});

router.delete("/:animeId", async (req, res) => {
  try {
    await Anime.findByIdAndRemove(req.params.animeId);

    return res.send({ message: "Anime deleted" });
  } catch (error) {
    return res.status(400).send({ error: "Error deleting anime." });
  }
});

module.exports = (app) => app.use("/animes", router);
