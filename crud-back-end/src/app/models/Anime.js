const mongoose = require("../../database");
const bcrypt = require("bcryptjs");

const AnimeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  situation: {
    type: String,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  numberEp: {
    type: Number,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const Anime = mongoose.model("Anime", AnimeSchema);

module.exports = Anime;
