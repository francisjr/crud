const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/crud");
mongoose.Promise = global.Promise;

module.exports = mongoose;
