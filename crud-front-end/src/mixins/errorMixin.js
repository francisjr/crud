export const errorMixin = {
  data: function () {
    return {
      msgError: undefined,
    };
  },

  methods: {
    configError: function (error) {
      if (error.response) {
        console.warn(error.response.data);
        this.msgError = error.response.data.error;
        setTimeout(() => (this.msgError = null), 1500);
      } else if (error.request) {
        console.warn(error.request);
      } else {
        console.warn("Error", error.message);
      }
    },
  },
};
