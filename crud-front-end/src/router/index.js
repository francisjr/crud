import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: () => import("@/views/Login.vue"),
    meta: {
      public: true,
    },
  },
  {
    path: "/home",
    name: "Home",
    component: () => import("@/views/Home.vue"),
  },
  {
    path: "/cadastro",
    name: "Registration",
    component: () => import("@/views/Registration.vue"),
    meta: {
      public: true,
    },
  },
  {
    path: "/anime/:id",
    name: "Anime Information",
    component: () => import("@/views/AnimeInfo.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (!to.meta.public && !store.state.token) {
    return next({ name: "Login" });
  }
  next();
});

export default router;
