import axios from "axios";
import store from "@/store";

const http = axios.create({
  baseURL: "http://localhost:3000/",
});

http.interceptors.request.use(function (config) {
  const token = store.state.token;

  config.headers.Authorization = `${token}`;
  return config;
});

export default http;
