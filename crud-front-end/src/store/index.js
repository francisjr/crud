import Vue from "vue";
import Vuex from "vuex";
import http from "@/services/config.js";

Vue.use(Vuex);

const state = {
  token: null,
  name: null,
};

const getters = {
  name: (state) => {
    return state.name;
  },
};

const mutations = {
  SET_LOGGED_USER(state, response) {
    state.token = `Bearer ${response.token}`;
    state.name = response.user.name;
  },

  LOGOUT_USER() {
    state.token = null;
  },
};

const actions = {
  login({ commit }, user) {
    return new Promise((resolve, reject) => {
      http
        .post("auth/signin", user)
        .then((response) => {
          let respData = response.data;
          commit("SET_LOGGED_USER", respData);
          resolve(response);
        })
        .catch((err) => {
          console.warn(reject(err));
        });
    });
  },

  register({ commit }, user) {
    return new Promise((resolve, reject) => {
      http
        .post("auth/register", user)
        .then((response) => {
          let respData = response.data;
          commit("SET_LOGGED_USER", respData);
          resolve(response);
        })
        .catch((err) => {
          console.warn(reject(err));
        });
    });
  },

  logout({ commit }) {
    commit("LOGOUT_USER");

    return true;
  },
};

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
});
